# Teste prático

Esse teste prático visa avaliar os conhecimentos do candidato a vaga de programador PHP 100% Remoto.

## Objetivos
  - Conhecer um pouco de suas habilidades em:
    - Laravel 5;
    - Entendimento e análise dos requisitos;
    - Capacidade de inovar;
    - Determinação na busca de soluções;
    - Responsabilidade na tomada de decisões.
    
## Escopo
Deve-se criar uma aplicação em PHP para resolver o problema descrito abaixo, utilizando framework Laravel 5. 
Fique a vontade para explorar todo o seu conhecimento em automação de tarefas, CSS e Javascript com JQuery ou qualquer
 outra ferramenta.
 

## Cenário fictício
A Netzee vai lançar um novo blog. Nesse blog, desejamos cadastrar categorias e posts através de um painel 
administrativo.
 

## Requisitos
- Um post pode ter mais de uma categoria.
- A consulta pelo nome é requisito funcional.
- É necessário autenticação.


#### CRUD de Categorias
Criar um gerenciamento aonde seja possível Criar, Listar, Editar e Visualizar uma categoria (Design, Programação, 
Marketing, por exemplo). 

##### Atributos de uma Categoria:
- título (obrigatório)
- status (obrigatório, ativo/inativo)
- descrição


#### CRUD de Posts
Criar um gerenciamento aonde seja possível Criar, Listar, Editar e Visualizar um post. 

##### Atributos de um Post:
- título (obrigatório)
- status (obrigatório, ativo/inativo)
- descrição (obrigatório)
- imagem de capa


## Instruções:

- No Bitbucket, faça fork desse repositório e adicione a conta "netzee-admin" ou "contato@netzee.com.br" para 
visualizar o mesmo. (Fica em Settings > User and group access > Users)
- Deve ser utilizado o Laravel como framework PHP
- Deve ser utilizado o Composer para gerenciar as dependências da aplicação. 
- Crie um README com orientações para a instalação.


## Plus ++ 
- Cubra pelo menos 3 recursos de seu código com testes.
- Utilize as melhores práticas da Orientação a Objetos.
- As tabelas do banco de dados criadas através de migrations.
- Utilizar Seeds para dados fictícios.
- Utilizar Docker (caso utilize subir as configurações utilizadas no repositório).
- Utilizar Redis para session.


## Observações:

- O que será avaliado é a qualidade do código não a velocidade de desenvolvimento. Portanto, qualquer 
generator / scaffolding de CRUD, MVC, etc, torna-se desnecessário. 
- Se não for possível terminar todas as funcionalidades, não tem problema.
- Não precisa ser complexo, com varias lib’s e etc. O legal é usar o necessário para ter um código de qualidade
 e de fácil evolução. 
- Lembrando código de qualidade, você pode e deve fazer o que achar necessário para isso, mesmo que não esteja listado
 aqui. 



Em caso de dúvidas, envie-nos um e-mail para contato@netzee.com.br